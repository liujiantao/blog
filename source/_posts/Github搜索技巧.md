---
title: Github搜索技巧
date: 2019-04-15 14:26:55
tags:
---

# Github搜索技巧
##1、Github Trend 和 GIthub Topic
[**github trend**](https://github.com/trending)
[**github topic** ](https://github.com/topics)

##2、搜索开发者


The following is a list of optional inline markups supported:

搜索条件         |        备注           |
--------------------|------------------|
location       | location:china 开发者个人信息填写的是china   |
language       | language:javascript   |
in:fullname    | jackma:fullname 名字是jackma的开发者
followers      | floowers:>=1000关注者大于1000的开发者  |
stars:         | stars:>=500收藏数量大于500的项目  |
forks      | forks:>=500 匹配分支数量大于500的项目  |
followers      | floowers:>=关注者大于1000  |
followers      | floowers:>=关注者大于1000  |

当然，这些搜索条件都可以组合使用.

github官方也给出了[**高级搜索功能**](https://github.com/search/advanced)，以及[**使用指南**](https://help.github.com/en/articles/searching-on-github)
